package com.aris.bahasapemrograman;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.aris.bahasapemrograman.model.ProgrammingLanguage;
import com.aris.bahasapemrograman.model.ProgrammingLanguagesData;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    private RecyclerView rvProgrammingLanguages;
    private ArrayList<ProgrammingLanguage> list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        rvProgrammingLanguages = findViewById(R.id.rv_programminglanguages);
        rvProgrammingLanguages.setHasFixedSize(true);

        list.addAll(ProgrammingLanguagesData.getListData());
        showRecylerList();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent moveToProfile = new Intent(this, Profile.class);
        startActivity(moveToProfile);
        return super.onOptionsItemSelected(item);
    }

    private void showRecylerList() {
        rvProgrammingLanguages.setLayoutManager(new LinearLayoutManager(this));
        ListProgrammingLanguageAdapter listProgrammingLanguageAdapter = new ListProgrammingLanguageAdapter(list);
        rvProgrammingLanguages.setAdapter(listProgrammingLanguageAdapter);

        listProgrammingLanguageAdapter.setOnItemClickCallback(new ListProgrammingLanguageAdapter.OnItemClickCallback() {
            @Override
            public void onItemClicked(ProgrammingLanguage data) {
                Intent moveToDetails = new Intent(MainActivity.this, Details.class);
                moveToDetails.putExtra(Details.EXTRA_IMAGE, data.getPhoto());
                moveToDetails.putExtra(Details.EXTRA_NAME, data.getName());
                moveToDetails.putExtra(Details.EXTRA_DETAILS, data.getDetails());
                startActivity(moveToDetails);
            }
        });
    }

}
