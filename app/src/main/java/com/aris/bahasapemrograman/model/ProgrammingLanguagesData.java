package com.aris.bahasapemrograman.model;

import com.aris.bahasapemrograman.R;

import java.util.ArrayList;

public class ProgrammingLanguagesData {
    private static String[] programmingLanguageNames = {
            "C",
            "C++",
            "Java",
            "C#",
            "PHP",
            "JavaScript",
            "Python",
            "Kotlin",
            "Swift",
            "Ruby"
    };

    private static String[] programmingLanguageDetails = {
            "C is a general-purpose, procedural computer programming language supporting structured programming, lexical variable scope, and recursion, while a static type system prevents unintended operations.",
            "C++ is a high-level, general-purpose programming language created by Bjarne Stroustrup as an extension of the C programming language, or \"C with Classes\".",
            "Java is a general-purpose programming language that is class-based, object-oriented, and designed to have as few implementation dependencies as possible.",
            "C# is a general-purpose, multi-paradigm programming language encompassing strong typing, lexically scoped, imperative, declarative, functional, generic, object-oriented, and component-oriented programming disciplines.",
            "PHP is a popular general-purpose scripting language that is especially suited to web development. It was originally created by Rasmus Lerdorf in 1994; the PHP reference implementation is now produced by The PHP Group.",
            "JavaScript, often abbreviated as JS, is a programming language that conforms to the ECMAScript specification. JavaScript is high-level, often just-in-time compiled, and multi-paradigm. It has curly-bracket syntax, dynamic typing, prototype-based object-orientation, and first-class functions.",
            "Python is an interpreted, high-level, general-purpose programming language. Created by Guido van Rossum and first released in 1991, Python's design philosophy emphasizes code readability with its notable use of significant whitespace.",
            "Kotlin is a cross-platform, statically typed, general-purpose programming language with type inference. Kotlin is designed to interoperate fully with Java, and the JVM version of its standard library depends on the Java Class Library, but type inference allows its syntax to be more concise.",
            "Swift is a general-purpose, multi-paradigm, compiled programming language developed by Apple Inc. for iOS, iPadOS, macOS, watchOS, tvOS, Linux, and z/OS. Swift is designed to work with Apple's Cocoa and Cocoa Touch frameworks and the large body of existing Objective-C code written for Apple products.",
            "Ruby is an interpreted, high-level, general-purpose programming language. It was designed and developed in the mid-1990s by Yukihiro \"Matz\" Matsumoto in Japan. Ruby is dynamically typed and uses garbage collection."
    };

    private static int[] programmingLanguageImages = {
            R.drawable.c,
            R.drawable.cpp,
            R.drawable.java,
            R.drawable.cs,
            R.drawable.php,
            R.drawable.js,
            R.drawable.py,
            R.drawable.kt,
            R.drawable.swift,
            R.drawable.rb
    };

    public static ArrayList<ProgrammingLanguage> getListData() {
        ArrayList<ProgrammingLanguage> list = new ArrayList<>();
        for (int position = 0; position < programmingLanguageNames.length; position++) {
            ProgrammingLanguage programmingLanguage = new ProgrammingLanguage();
            programmingLanguage.setName(programmingLanguageNames[position]);
            programmingLanguage.setDetails(programmingLanguageDetails[position]);
            programmingLanguage.setPhoto(programmingLanguageImages[position]);
            list.add(programmingLanguage);
        }
        return list;
    }
}
