package com.aris.bahasapemrograman;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.aris.bahasapemrograman.model.ProgrammingLanguage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

import java.util.ArrayList;

public class ListProgrammingLanguageAdapter extends RecyclerView.Adapter<ListProgrammingLanguageAdapter.ListViewHolder> {
    private OnItemClickCallback onItemClickCallback;
    private ArrayList<ProgrammingLanguage> listProgrammingLanguage;

    public ListProgrammingLanguageAdapter(ArrayList<ProgrammingLanguage> list) {
        this.listProgrammingLanguage = list;
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row_programminglanguage, parent, false);
        return new ListViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull final ListViewHolder holder, int position) {
        ProgrammingLanguage programmingLanguage = listProgrammingLanguage.get(position);
        Glide.with(holder.itemView.getContext())
                .load(programmingLanguage.getPhoto())
                .apply(new RequestOptions().override(55, 55))
                .into(holder.imgPhoto);
        holder.tvName.setText(programmingLanguage.getName());
        holder.tvDetails.setText(programmingLanguage.getDetails());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onItemClickCallback.onItemClicked(listProgrammingLanguage.get(holder.getAdapterPosition()));
            }
        });
    }

    @Override
    public int getItemCount() {
        return listProgrammingLanguage.size();
    }

    public void setOnItemClickCallback(OnItemClickCallback onItemClickCallback) {
        this.onItemClickCallback = onItemClickCallback;
    }

    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgPhoto;
        TextView tvName, tvDetails;

        public ListViewHolder(@NonNull View itemView) {
            super(itemView);
            imgPhoto = itemView.findViewById(R.id.img_item_photo);
            tvName = itemView.findViewById(R.id.tv_item_name);
            tvDetails = itemView.findViewById(R.id.tv_item_details);
        }
    }

    public interface OnItemClickCallback {
        void onItemClicked(ProgrammingLanguage data);
    }
}
