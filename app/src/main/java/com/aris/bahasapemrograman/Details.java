package com.aris.bahasapemrograman;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.aris.bahasapemrograman.model.ProgrammingLanguage;
import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;

public class Details extends AppCompatActivity {
    public static final String EXTRA_IMAGE = "extra_image";
    public static final String EXTRA_NAME = "extra_name";
    public static final String EXTRA_DETAILS = "extra_details";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);

        ImageView detailPhoto = findViewById(R.id.img_detail_photo);
        TextView tvDetailName = findViewById(R.id.tv_detail_name);
        TextView tvDetails = findViewById(R.id.tv_details);

        int image = getIntent().getIntExtra(EXTRA_IMAGE, 0);
        String name = getIntent().getStringExtra(EXTRA_NAME);
        String details = getIntent().getStringExtra(EXTRA_DETAILS);

        Glide.with(this)
                .load(image)
                .apply(new RequestOptions())
                .into(detailPhoto);
        tvDetailName.setText(name);
        tvDetails.setText(details);
    }
}
